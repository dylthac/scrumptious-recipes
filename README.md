# Scrumptious Recipes

## Installation:
1. Open your terminal
2. CD into desired folder
3. Enter the following commands into the terminal:
    - git clone https://gitlab.com/dylthac/scrumptious-recipes.git
    - cd scrumptious-recipes
    - python -m venv .venv
    - .\\\.venv\Scripts\Activate.ps1
    - pip install -r requirements.txt
    - python manage.py runserver
4. Once the development server is up, visit http://127.0.0.1:8000/


## To Use:


**Tags**
Tags are used to give recipes flags or filters to easily search for the recipe in the future, and to sort similar recipes. To use them, click the "tags" tab, from there you can click "edit" to change the tag, or "write a tag" to create a new one.

**Meal Plans**
Meal Plans allow you to schedule out your meals for days in advanced. To use it, click the "meal plans" tab, then "create", and use the form to create your plan.

**Write a Recipe**
To write a recipe, simply click the "write a recipe" tab and fill out the form, submitting will allow it to show on the main page.

**Login/Logout**
When logged out, you'll only be able to see the main scrumptious page, when clicking on any of the other tabs you'll be prompted to log in. Once logged in you'll have access to all tabs, and the logout tab will appear as well.

### Overview:
Scrumptious is a great way to store recipes and keep them organized in one consolidated place. Able to scale recipes down by the serving size and even sort recipes by type.